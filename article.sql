-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               10.1.37-MariaDB - mariadb.org binary distribution
-- Операционная система:         Win32
-- HeidiSQL Версия:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных article
DROP DATABASE IF EXISTS `article`;
CREATE DATABASE IF NOT EXISTS `article` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `article`;

-- Дамп структуры для таблица article.articles
DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(200) NOT NULL,
  `body` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы article.articles: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT IGNORE INTO `articles` (`article_id`, `title`, `body`, `user_id`, `created`, `modified`) VALUES
	(9, 'Братан z', 'aaaa a aa  a df fgd gdgdfg ggf df  fg dff df dfgd fgcccccccccccccccccccccccccc', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(10, 'Братан v', 'vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv', 11, NULL, NULL),
	(17, 'Insert() при работе с DAO', '//Соединение с базой данных\r\n$connection = Yii::$app->db;\r\n// выполняем команду вставки (параметры таблица Country, массив с данными) и выполняем запрос\r\n$connection->createCommand()->insert(\'Country\', [\'name\' => \'Англия\', \'number\' => \'53012456\', \'area\' => \'133396\'])->execute();', 16, '2019-08-04 11:26:28', '2019-08-04 11:26:28'),
	(18, 'Удаление данныхссс', 'Для удаления одной отдельной строки данных сначала получите Active Record объект, соответствующий этой строке, а затем вызовите метод yii\\db\\ActiveRecord::delete().\r\n\r\n$customer = Customer::findOne(123);\r\n$customer->delete();\r\nВы можете вызвать yii\\db\\ActiveRecord::deleteAll() для удаления всех или нескольких строк данных одновременно. Например:', 24, '2019-08-04 11:28:56', '2019-08-04 23:25:57'),
	(22, 'Жизненный цикл сохранения данных', 'ddddddddddddddddddddddddddddddddddddddddddddddddddd', 24, '2019-08-04 23:47:49', '2019-08-04 23:47:49'),
	(24, 'Атрибуты name и value для полей ввода', 'Также имеются методы для получения значений атрибутов id, name и value для полей ввода, сформированных на основе моделей. Эти методы используются в основном внутренними механизмами, но иногда могут оказаться подходящими и для прямого использования:', 28, '2019-08-05 00:46:34', '2019-08-05 00:46:34'),
	(25, 'Жизненный цикл сохранения данных', '21111111111111111111111111000000000000000000001111111111111111111111111111', 24, '2019-08-05 08:22:57', '2019-08-05 08:23:13'),
	(27, 'Код состояния', 'Первое, что вы делаете при построении ответа, — определяете, был ли\r\nуспешно обработан запрос. Это реа00000000лизуется заданием свойству yii\\web\r\n\\Response::$statusCode значения, которое может быть одним из валидных HTTP-кодов состояния4\r\n. Например, чтобы показать, что запрос\r\nбыл успешно обработан, вы можете установить значение кода состояния\r\nравным 200:', 29, '2019-08-05 08:31:54', '2019-08-05 09:09:57');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;

-- Дамп структуры для таблица article.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` char(250) NOT NULL,
  `name` char(250) NOT NULL,
  `password` char(100) NOT NULL,
  `role` enum('admin','user') NOT NULL DEFAULT 'user',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы article.users: ~9 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT IGNORE INTO `users` (`id`, `email`, `name`, `password`, `role`, `created`, `modified`) VALUES
	(16, 'mralat@gmail.com', 'Алекс', '$2y$13$lEg8VREGtXvr4MD/pqoicuKtycNZerl3WQyJlY6tPaN0HPZhqRZwC', 'admin', '0000-00-00 00:00:00', '2019-08-05 00:27:19'),
	(17, 'yy@ccc.com', 'Хобс', '$2y$13$d4Ku2Mz77GgHAn7AQL3SzOERC54Lo13XDGEU8pDzmiQFPD7MKaqTK', 'user', '2019-08-04 12:35:01', '2019-08-05 00:27:25'),
	(18, 'ttt@ggg.com', 'Шоу', '$2y$13$6eUr8m.dQKo0sYW8vivLKe/Tm3Dsyr58WIM7DErglD1rYj4Eaj9U6', 'admin', '2019-08-04 12:36:21', '2019-08-05 00:27:31'),
	(20, 'dddddddd@ttt.v', 'Спартак', '$2y$13$CVrgYP2u8XQWvTUgdkM9KufRtl.NEy4XFfJdQKvkHaGOIbl3BcVG6', 'user', '2019-08-04 12:39:40', '2019-08-05 00:27:58'),
	(21, 'miralig@gmail.co', 'Джон', '$2y$13$LtYIX9wcohBsJ9VnrptBROhR5zfqH2iZzwPFrDeGxX1zbFJyl2Iaq', 'user', '2019-08-04 12:41:08', '2019-08-05 00:27:38'),
	(22, 'ra2ligat@gmail.com', 'Бредд', '$2y$13$PJafU2P/iIsQiolA18St8enJH0M6iFkjd738x61qeBn/LlmAoZ8qK', 'admin', '2019-08-04 13:02:41', '2019-08-05 00:27:45'),
	(23, 'raligddddat@gmail.co', 'Берн', '$2y$13$FHwaTIIcLP6naqxmv6nLk.6nompGkP/0TORbPY7E1hRePg1P2MikS', 'user', '2019-08-04 13:04:55', '2019-08-05 00:28:14'),
	(24, 'miraligat@gmail.com', 'Мирлан', '$2y$13$Kkirb5qbAYuuomomrKPnRe2FnWxpj5uvNVkPoUJm0dQd83p0Ete8a', 'admin', '2019-08-04 13:07:17', '2019-08-05 00:48:34'),
	(27, 'vv@gmail.com', 'Godder', '$2y$13$bzePya.8I5BI6DFnqEUDV./y0aRRYoyzLl4i/FZb5U31YjavIdrR2', 'user', '2019-08-05 00:34:07', '2019-08-05 00:34:07'),
	(28, 'ho@gmail.com', 'Шойгу', '$2y$13$221skvCRd773mMOIoTTt5OjSXRKl6AQ2ip.XFk6/hUWYcgjSTMMEO', 'user', '2019-08-05 00:45:47', '2019-08-05 00:45:47'),
	(29, 'bracho@gmail.com', 'Bracho', '$2y$13$41DzQG2bZ8DcK/ozoCopC.kMl8v6xTuprQGNE7MG6Ff.4cRzoRSaS', 'user', '2019-08-05 08:24:42', '2019-08-05 08:24:42');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

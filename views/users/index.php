<?php
use yii\helpers\Html;
$this->params['breadcrumbs'][] = $this->title;
?>
<?= Html::a('Добавить пользователя', ['add'])?>
<h1><?= Html::encode($this->title) ?></h1>
<table class="table">
    <thead>
    <tr>
        <th scope="col">Id</th>
        <th scope="col">E-mail</th>
        <th scope="col">Имя</th>
        <th scope="col">Роль</th>
        <th scope="col">Создан</th>
        <th scope="col">Отредактирован</th>
        <th scope="col">Операции</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach ($users as $user) { ?>
        <tr>
            <th scope="row"><?= $user->id ?></th>
            <td><?= Html::a(Html::encode($user->email), ['view', 'id' => $user->id]) ?></td>
            <td><?= Html::a(Html::encode($user->name), ['view', 'id' => $user->id]) ?></td>
            <td><?= Html::encode($user->role) ?></td>
            <td><?= $user->created ?></td>
            <td><?= $user->modified ?></td>
            <td><?= Html::a('Edit', ['edit', 'id' => $user->id])?>

                <?php echo Yii::$app->user->identity->getId() != $user->id ? ' | '. Html::a('Delete', ['delete', 'id' => $user->id]) : ''?></td>
        </tr>
    <?php }?>

    </tbody>
</table>



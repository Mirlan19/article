<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->params['breadcrumbs'][] = $this->title;
?>

<div class="users-add">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin()?>
    <?= $form->field($addForm, 'email')->input('email') ?>
    <?= $form->field($addForm, 'name')?>
    <?= $form->field($addForm, 'password')->passwordInput() ?>
    <?= $form->field($addForm, 'password_repeat')->passwordInput() ?>
    <?= $form->field($addForm, 'role')->dropDownList(['user' => 'Пользователь', 'admin' => 'Администратор']) ?>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary'])?>
    <?php $form = ActiveForm::end()?>

</div>
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->params['breadcrumbs'][] = $this->title;
?>

<div class="users-edit">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin()?>
    <?= $form->field($editForm, 'id')->hiddenInput()->label(false) ?>
    <?= $form->field($editForm, 'email')->input('email') ?>
    <?= $form->field($editForm, 'name')?>
    <?= $form->field($editForm, 'password')->passwordInput() ?>
    <?= $form->field($editForm, 'password_repeat')->passwordInput() ?>
    <?= $form->field($editForm, 'role')->dropDownList(['user' => 'Пользователь', 'admin' => 'Администратор']) ?>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary'])?>
    <?php $form = ActiveForm::end()?>

<!--    --><?//= $this->registerCssFile('@web/js/script.js', ['depends' => [\yii\web\JqueryAsset::className()]]) ?>

</div>
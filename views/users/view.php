<?php
use yii\helpers\Html;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="users-view">
    <div class="contain">
        <h3 class="mt-5">Имя: <?= Html::encode($user->name) ?></h3>
        <h3 class="mt-5">E-mail: <?= Html::encode($user->email) ?></h3>

        <p>Дата регистрации: <?= $user->created ?></p>

        <p>Дата редактирования: <?= $user->modified ?></p>
        <p><?= Html::a('Назад к списку пользователей', ['index']) ?></p>
    </div>
    <p>Количество статей: <?= count($user->articles) ?></p>
    <?php if ($user->articles) { ?>
    <h1>Статьи</h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Название</th>
            <th scope="col">Автор</th>
            <th scope="col">Создан</th>
            <th scope="col">Отредактирован</th>
            <th scope="col">Операции</th>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($user->articles as $article) { ?>
            <tr>
                <th scope="row"><?= $article->article_id ?></th>
                <td><?= Html::a(Html::encode($article->title), ['articles/view', 'id' => $article->article_id]) ?></td>
                <td><?= Html::encode(empty($article->author) ? 'deleted' : $article->author->email ) ?></td>
                <td><?= $article->created ?></td>
                <td><?= $article->modified ?></td>
                <td><?= Html::a('Edit', ['articles/edit', 'id' => $article->article_id])?> | <?= Html::a('Delete', ['articles/delete', 'id' => $article->article_id])?></td>
            </tr>
        <?php }?>

        </tbody>
    </table>
</div>
<?php } ?>


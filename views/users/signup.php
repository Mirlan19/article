<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="users-signup">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>Пожалуйста, заполните следующие поля для регистрации:</p>
    <?php $form = ActiveForm::begin()?>
    <?= $form->field($signUpForm, 'email')->input('email') ?>
    <?= $form->field($signUpForm, 'name')?>
    <?= $form->field($signUpForm, 'password')->passwordInput() ?>
    <?= $form->field($signUpForm, 'password_repeat')->passwordInput() ?>
<!--    --><?//= $form->field($signUpForm, 'role')->dropDownList(['user' => 'Пользователь', 'admin' => 'Администратор']) ?>
    <?= Html::submitButton('Регистрация', ['class' => 'btn btn-primary'])?>
    <?php $form = ActiveForm::end()?>

</div>
<?php
/**
 * Created by PhpStorm.
 * User: MirlanDev
 * Date: 03.08.2019
 * Time: 20:47
 */
use yii\helpers\Html;

$this->params['breadcrumbs'][] = $this->title;
?>

<div class="articles-view">
    <div class="contain">
        <h2 class="mt-5"><?= Html::encode($article->title) ?></h2>

        <p class="lead"><?= Html::encode($article->body) ?></p>

        <p>Дата создания: <?= $article->created ?> | Автор: <?= Html::encode(empty($article->author)  ? 'deleted' : $article->author->name) ?></p>
        <?= Html::a('Назад к статьям', ['index']) ?>
    </div>
</div>
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articles-add">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin()?>
    <?= $form->field($addForm, 'title') ?>
    <?= $form->field($addForm, 'body')->textarea(['rows' => 4]) ?>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary'])?>
    <?php $form = ActiveForm::end()?>
</div>
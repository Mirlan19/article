<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->params['breadcrumbs'][] = $this->title;
?>

<div class="articles-edit">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin(['id' => 'edit-form'])?>
    <?= $form->field($editForm, 'title') ?>
    <?= $form->field($editForm, 'body')->textarea(['rows' => 4]) ?>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary'])?>
    <?php $form = ActiveForm::end()?>
</div>
<?php
use yii\helpers\Html;
$this->params['breadcrumbs'][] = $this->title;
?>
<?= Html::a('Добавить статью', ['add'])?>

<h1><?= Html::encode($this->title) ?></h1>
<table class="table">
    <thead>
    <tr>
        <th scope="col">Id</th>
        <th scope="col">Название</th>
        <th scope="col">Автор</th>
        <th scope="col">Создан</th>
        <th scope="col">Отредактирован</th>
        <th scope="col">Операции</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach ($articles as $article) { ?>
    <tr>
        <th scope="row"><?= $article->article_id ?></th>
        <td><?= Html::a(Html::encode($article->title), ['view', 'id' => $article->article_id]) ?></td>
        <td><?= Html::encode(empty($article->author) ? 'deleted' : $article->author->name ) ?></td>
        <td><?= $article->created ?></td>
        <td><?= $article->modified ?></td>
        <td><?= Html::a('Edit', '#'/*['edit', 'id' => $article->article_id]*/, ['class' => 'article-edit-button', 'article_id' => $article->article_id, 'data-toggle'=> "modal", 'data-target' => "#articleEditModal"])?>
            | <?= Html::a('Delete', ['delete', 'id' => $article->article_id, 'confirm' => 'Are you sure ?'])?></td>
    </tr>
    <?php }?>

    </tbody>
</table>

<?= $this->registerJsFile('@web/js/script.js', ['depends' => [\yii\web\JqueryAsset::className()]]) ?>

<!-- Кнопка пуска модальное окно -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
    Launch demo modal
</button>

<!-- Модальное окно -->
<div class="modal fade" id="articleEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
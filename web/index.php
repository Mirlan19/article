<?php

if (!function_exists('print_arr')) {
    function print_arr($var, $return = false) {
        $type = gettype($var);

        $out = print_r($var, true);
        $out = htmlspecialchars($out);
        $out = str_replace(' ', '&nbsp; ', $out);
        if ($type == 'boolean')
            $content = $var ? 'true' : 'false';
        else
            $content = nl2br( $out );

        $out = '<div style="
       border:2px inset #666;
       background:black;
       font-family:Verdana;
       font-size:11px;
       color:#6F6;
       text-align:left;
       margin:20px;
       padding:16px">
         <span style="color: #F66">('.$type.')</span> '.$content.'</div><br /><br />';

        if (!$return)
            echo $out;
        else
            return $out;
    }
}

if (!function_exists('print_die')) {
    function print_die($var, $return = false)
    {
        print_arr($var, $return);
        die;
    }
}
// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

(new yii\web\Application($config))->run();



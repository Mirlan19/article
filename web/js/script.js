var baseUrl = window.location.origin;

$('.article-edit-button').click(function () { // ������ ������ ����� id ����� ��������

    var article_id = ($(this).attr('article_id'));
    $.ajax({
        type: "GET",
        url: baseUrl + '/article/web/articles/edit',
        data: {id:article_id},
        success: function(data){
            // data ��� ����� html ���� ��� ����� �������������� �������
            //console.log(data);

            $('#articleEditModal').modal('show')
                .find('.modal-body') // ������� ���� �������
                .html(data); // ��������� html
                //.modal('show');// ��������� �������

        }
    });
});

$(document.body).on('submit', '#edit-form', function (e) {

    console.log(e);

    e.preventDefault(); // ��������� �������� �����;
    $.ajax({
        type: "POST",
        url: this.action, // ��� action ����� � ����� ������ ��� ������ ���� ������ ���� 'http://localhost/article/web/articles/edit?id=28',
        data: $(this).serialize(), // ������ ������
        success: function(data){
            // ������ �� ��� ����� ������� ����� �������������� (�������� ������ ����������������� ������  � ������ ��������)
            $('#articleEditModal').modal();// ��������� �������
        }
    });
});

<?php
/**
 * Created by PhpStorm.
 * User: MirlanDev
 * Date: 02.08.2019
 * Time: 18:48
 */

namespace app\models;

use yii\base\Model;

class TestForm extends  Model
{
    public $name;
    public $email;
    public $text;

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => 'Email',
            'text' => 'Текст сообщения'
        ];
    }

    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            ['email', 'email'],
            ['name', 'string', 'min' => 2],
            ['name', 'string', 'length' => [2,5]],
            ['text', 'trim']
        ];
    }

}
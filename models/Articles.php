<?php
/**
 * Created by PhpStorm.
 * User: MirlanDev
 * Date: 03.08.2019
 * Time: 20:59
 */

namespace app\models;


use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
class Articles extends ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => 'modified',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function isOwner()
    {
        if (\Yii::$app->user->identity->getId() == $this->user_id)
            return true;
        return false;
    }

}
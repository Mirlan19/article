<?php
/**
 * Created by PhpStorm.
 * User: MirlanDev
 * Date: 03.08.2019
 * Time: 20:53
 */

namespace app\models;


use yii\base\Model;

class ArticleForm extends Model
{
    public $title;
    public $body;

    public function attributeLabels()
    {
        return [
            'title' => 'Заголовок',
            'body' => 'Текст',
        ];
    }

    public function rules()
    {
        return [
            [['title', 'body'], 'required'],
            ['title', 'string', 'max' => 200],
            ['body', 'string', 'max' => 1000],
        ];
    }

    public function save($article = null)
    {
        if (empty($article))
            $article = new Articles();

        $article->title = $this->title;
        $article->body = $this->body;
        $article->user_id = \Yii::$app->user->identity->getId();
        return $article->save();
    }


}
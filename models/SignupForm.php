<?php
/**
 * Created by PhpStorm.
 * User: MirlanDev
 * Date: 03.08.2019
 * Time: 11:43
 */

namespace app\models;
use Yii;
use yii\base\Model;

class SignupForm extends Model
{
    public $email;
    public $name;
    public $password;
    public $password_repeat;

    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'name' => 'Имя',
            'password' => 'Пароль',
            'password_repeat' => 'Повторите пароль',
        ];
    }

    public function rules()
    {

        return [
            [['email', 'name', 'password', 'password_repeat'], 'required'],
            ['email', 'email'],
            ['name', 'string', 'min' => 2, 'max' => 50],
            [
                'email',
                'unique',
                'targetClass' => 'app\models\User',
            ],
            ['password', 'string', 'min' => 2, 'max' => 10],
            ['password_repeat', 'string', 'min' => 2, 'max' => 10],
            ['password_repeat', 'compare', 'compareAttribute' => 'password'],
            ['password', 'compare', 'compareAttribute' => 'password_repeat'],
        ];
    }

     public function save()
     {
         $user = new User();
         $user->email = $this->email;
         $user->name = $this->name;
         $user->setPassword($this->password);
         $user->save();
         return $user;
     }
}
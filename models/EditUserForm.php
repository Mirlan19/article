<?php
/**
 * Created by PhpStorm.
 * User: MirlanDev
 * Date: 04.08.2019
 * Time: 21:00
 */

namespace app\models;

use Yii;

use yii\base\Model;

class EditUserForm extends Model
{
    public $id;
    public $email;
    public $name;
    public $password;
    public $password_repeat;
    public $role;

    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'name' => 'Имя',
            'password' => 'Пароль',
            'password_repeat' => 'Повторите пароль',
            'role' => 'Роль'
        ];
    }

    public function rules()
    {
//        print_die($this->attributes);

        return [
            ['id', 'integer'],
            [['email', 'name'], 'required'],
            ['name', 'string', 'min' => 2, 'max' => 50],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => 'app\models\User',
                'when' => function ($model) {
                    $user = User::findOne($model->id);
                    if ($user && $user->email == $model->email)
                        return false;
                    else
                        return true;
                }
            ],
//            ['password', 'compare'],
            ['password', 'string', 'min' => 2, 'max' => 10],
            ['password_repeat', 'string', 'min' => 2, 'max' => 10],
            ['password', 'compare', 'compareAttribute' => 'password_repeat'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password'],
            ['role', 'in', 'range' => ['user', 'admin']],
        ];
    }


    public function save($user)
    {
//        $g = User::find()->where('id != ' . $user->id)->where(['email' => $this->email]);
//        print_die($g);
        $user->email = $this->email;
        $user->name = $this->name;
        if ($this->password) $user->setPassword($this->password);
        $user->role = $this->role;
        $user->save();
        return $user;
    }


}
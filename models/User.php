<?php
/**
 * Created by PhpStorm.
 * User: MirlanDev
 * Date: 02.08.2019
 * Time: 21:08
 */

namespace app\models;


use yii\db\ActiveRecord;
use Yii;
use yii\web\IdentityInterface;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

class User extends ActiveRecord implements IdentityInterface
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => 'modified',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public static function tableName()
    {
        return 'users';
    }

    public function getArticles()
    {
        return $this->hasMany(Articles::className(), ['user_id' => 'id']);
    }

    public function isAdmin()
    {
        if ($this->role == 'admin')
            return true;
        return false;
    }

    public function setPassword($password)
    {
        $this->password = \Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    public function validatePassword($password)
    {
//        print_die($password);
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {

    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {

    }

    public function validateAuthKey($authKey)
    {

    }
}
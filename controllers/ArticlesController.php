<?php
/**
 * Created by PhpStorm.
 * User: MirlanDev
 * Date: 03.08.2019
 * Time: 20:04
 */

namespace app\controllers;


use app\models\ArticleForm;
use app\models\Articles;
use app\models\User;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use Yii;
class ArticlesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'add', 'view', 'edit', 'delete'],
                'rules' => [

                    [
                        'allow' => true,
                        'actions' => ['index', 'add', 'view', 'edit', 'delete'],
                        'roles' => ['@'],

                        'denyCallback' => function ($rule, $action) {
                            throw new \Exception('У вас нет доступа к этой странице');
                        }

                    ],

                ],

            ],
        ];
    }

    public function actionIndex()
    {
        $articles = null;
        $viewer = \Yii::$app->user->identity;

        if ($viewer->isAdmin())
            $articles = Articles::find()->with('author')->all();
        else
            $articles = Articles::find()->with('author')->where(['user_id' => $viewer->getId()])->all();
//        print_die($articles);
        $this->view->title = 'Список статей';
        return $this->render('index', compact('articles'));
    }

    public function actionView($id)
    {
        $id = (int)$id;
        $article = Articles::findOne($id);
        if (empty($article)) {
            throw new NotFoundHttpException('Статья не найдена');
        }

        if (!Yii::$app->user->identity->isAdmin() && !$article->isOwner()) {
            throw new ForbiddenHttpException('У вас нет доступа к этой странице');
        }
        $this->view->title = 'Просмотр статьи';
        return $this->render('view', compact('article'));
    }

    public function actionAdd()
    {
        $addForm = new ArticleForm();
        if ($addForm->load(\Yii::$app->request->post())) {
            if ($addForm->validate()) {

                if( $addForm->save()) {
                    Yii::$app->session->setFlash('success', "Статья успешно добавлена");
                } else
                    Yii::$app->session->setFlash('error', "Ошибка добавления статьи");
                return $this->redirect(['index']);
            }
        }
        $this->view->title = 'Добавление статьи';
        return $this->render('add', compact('addForm'));
    }

    public function actionEdit($id)
    {
        $this->view->title = 'Редактирование статьи';
        $id = (int)$id;
        $article = Articles::findOne($id);

        if (empty($article)) {
            throw new NotFoundHttpException('Статья не найдена');
        }
        if (!Yii::$app->user->identity->isAdmin() && !$article->isOwner()) {
            throw new ForbiddenHttpException('У вас нет доступа к этой странице', 403);
        }

        if (\Yii::$app->request->isAjax) {
            $this->layout = false;
        }

        $editForm = new ArticleForm();
        $editForm->setAttributes($article->toArray());
        if ($editForm->load(\Yii::$app->request->post())) {
            if ($editForm->validate()) {
                if ($editForm->save($article)) {
                    Yii::$app->session->setFlash('success', "Статья успешно отредактирована");
                } else
                    Yii::$app->session->setFlash('error', "Ошибка редактирования статьи");

                return $this->redirect(['index']);
            }
        }
        return $this->render('edit', compact('editForm'));
    }

    public function actionDelete($id)
    {
        $id = (int)$id;
        $article = Articles::findOne($id);
        if (empty($article)) {
            throw new NotFoundHttpException('Статья не найдена');
        }

        if (!Yii::$app->user->identity->isAdmin() && !$article->isOwner()) {
            throw new ForbiddenHttpException('У вас нет доступа к этой странице');
        }

        if ($article->delete())
            \Yii::$app->session->setFlash('success', 'Статья успешно удалена');
        else
            \Yii::$app->session->setFlash('error', 'Ошибка удаления статьи');

        return $this->redirect('index');
    }


}
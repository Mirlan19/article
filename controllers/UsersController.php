<?php
/**
 * Created by PhpStorm.
 * User: MirlanDev
 * Date: 02.08.2019
 * Time: 14:02
 */

namespace app\controllers;

use app\models\Articles;
use app\models\EditUserForm;
use yii\filters\AccessControl;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\User;
use yii\web\Controller;
use app\models\TestForm;
use yii\web\NotFoundHttpException;
use Yii;

class UsersController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout', 'signup', 'index', 'view', 'edit', 'add', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['login', 'logout'],
                        'roles' => ['@'],
                    ],

                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'edit', 'add', 'delete'],
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->isAdmin();
                        },
///                        'denyCallback' => function ($rule, $action) {
//                            throw new \Exception('У вас нет доступа к этой странице');
//                        }

                    ],

                ],

            ],
        ];
    }

    public function actionIndex()
    {
        $viewer = Yii::$app->user->identity;
        $users = User::find()
//            ->where('id != ' . $viewer->getId())
            ->all();

        $this->view->title = 'Список пользователей';
        return $this->render('index', compact('users'));
    }

    public function actionSignup()
    {
        $signUpForm = new SignupForm();
        if ($signUpForm->load(Yii::$app->request->post())) {
            if ($signUpForm->validate()) {
                Yii::$app->user->login($signUpForm->save());
                return $this->goHome();
            }
        }
        $this->view->title = "Регистрация";
        return $this->render('signup', compact('signUpForm'));
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest)
            $this->goHome();

        $loginForm = new LoginForm();

        if ($loginForm->load(Yii::$app->request->post())) {
            if ($loginForm->validate()) {
                Yii::$app->user->login($loginForm->getUser());
                $this->goHome();
//                print_die('validate');
            }
        }
        $this->view->title = "Авторизация";
        return $this->render('login', compact('loginForm'));
    }

    public function actionLogout()
    {
        if (!Yii::$app->user->isGuest) {
            Yii::$app->user->logout();
        }
        return $this->redirect(['login']);
    }

    public function actionDelete($id)
    {
        $id = (int)$id;
        $user = User::findOne($id);
        $viewer = \Yii::$app->user->identity;
        if (empty($user)) {
            throw new NotFoundHttpException('Пользователь не найден');
        }
        //Самоудаление
        if ($viewer->getId() == $user->id) {
            \Yii::$app->session->setFlash('error','Данное действие невозможно!');
            return $this->redirect(['index']);
        }

        //Удаление пользователя
        if ($user->delete()) {
            Articles::deleteAll(['user_id' => $user->id]);
            \Yii::$app->session->setFlash('success', "Пользователь $user->email успешно удален");
        }
        else
            \Yii::$app->session->setFlash('error', 'Ошибка удаления пользователя');

        return $this->redirect(['index']);
    }

    public function actionAdd()
    {
        $addForm = new SignupForm();
        if ($addForm->load(Yii::$app->request->post())) {
            if ($addForm->validate()) {
                if ($user = $addForm->save())
                    Yii::$app->session->setFlash('success', "Пользователь $user->email успешно добвален");
                else
                    Yii::$app->session->setFlash('error', 'Ошибка при добавлении пользователя');
                return $this->redirect(['index']);
            }
        }
        $this->view->title = 'Добавление пользвателя';
        return $this->render('add', compact('addForm'));
    }

    public function actionEdit($id)
    {
        $id = (int)$id;
        $user = User::findOne($id);
        $viewer = \Yii::$app->user->identity;

        if (empty($user)) {
            throw new NotFoundHttpException('Пользователь не найден');
        }

        $editForm = new EditUserForm();
        $userArr = $user->toArray();
        unset($userArr['password']);

        $editForm->setAttributes($userArr);
//        print_die($editForm->attributes);
        if ($editForm->load(Yii::$app->request->post())) {
            if ($editForm->validate()) {
                if ($user = $editForm->save($user))
                    Yii::$app->session->setFlash('success', "Пользователь $user->email успешно отредактирован");
                else
                    Yii::$app->session->setFlash('error', 'Ошибка при редактировании пользователя');
                return $this->redirect(['index']);
            }
        }

        $this->view->title = 'Редактирование пользователя';
        return $this->render('edit', compact('editForm'));
    }

    public function actionView($id)
    {
        $id = (int)$id;
        $user = User::findOne($id);

        if (empty($user)) {
            throw new NotFoundHttpException('Пользователь не найден');
        }
        $this->view->title = 'Просмотр пользователя';
        return $this->render('view', compact('user'));
    }

}